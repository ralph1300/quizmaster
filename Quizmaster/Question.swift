//
//  Question.swift
//  Quizmaster
//
//  Created by Ralph Schnalzenberger on 16.11.15.
//  Copyright © 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

class Question {
    private var questionTitle:String;
    private var answers:[Answer];
    
    init(_questionTitle:String, _answers:[Answer]){
        questionTitle = _questionTitle;
        answers = _answers;
    }
    init(){
        questionTitle = "";
        answers = [];
    }
    func getQuestionTitle()->String{ return questionTitle; }
    func setQuestionTitle(_questionTitle:String){ questionTitle = _questionTitle; }
    func getAnswers()->[Answer]{ return answers; }
    func setAnswers(_answers:[Answer]){ answers = _answers; }
}