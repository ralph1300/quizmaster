//
//  GameViewController.swift
//  Quizmaster
//
//  Created by Ralph Schnalzenberger on 20.11.15.
//  Copyright © 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation
import UIKit

class GameViewController: UIViewController {
    
    // All the elements we need from the View
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var answerOneButton: UIButton!
    @IBOutlet weak var answerTwoButton: UIButton!
    @IBOutlet weak var answerThreeButton: UIButton!
    @IBOutlet weak var answerFourButton: UIButton!
    @IBOutlet weak var jokerButton: UIButton!
    
    // Mark if the joker was used
    var jokerWasUsed:Bool = false;
    var questionsWrong:Int = 0;
    // The question-array that was parsed in the ViewController
    var questionList:Array<Question> = [];
    var currentQuestion:Question = Question.init();
    // Array for the indexes of the already asked questions
    var alreadyAsked:Array<Int> = [];
    var buttonArray:Array<UIButton> = [];
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        getButtonsIntoArray();
        //progressLabel.text = "1" + "/" + String(questionList.count);
        loadQuestion();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func getButtonsIntoArray(){
        buttonArray.append(answerOneButton);
        buttonArray.append(answerTwoButton);
        buttonArray.append(answerThreeButton);
        buttonArray.append(answerFourButton);
    }

    // Connect all four answerButtons to this method by ctrl+dragging on the method
    @IBAction func answerPressed(sender: AnyObject) {
        let btnPressed = (sender) as! UIButton;
        let btnTitle = btnPressed.currentTitle;
        let tag = btnPressed.tag;
        var isAnswerCorrect:Bool = false;
        if(tag != 1){
            for(var j = 0; j < currentQuestion.getAnswers().count; j++){
                if(currentQuestion.getAnswers()[j].getText() == btnTitle && currentQuestion.getAnswers()[j].getIsRight()){
                    // Answer is correct
                    loadQuestion();
                    isAnswerCorrect = true;
                }
            }
            if(!isAnswerCorrect){
                questionsWrong++;
                // Show an alert that the answer is wrong
                let alert:UIAlertController = UIAlertController.init(title: "Sorry", message: "Diesmal war die Antwort falsch, versuch's nochmal \n" + String(questionsWrong) + "/3 falsch" , preferredStyle: UIAlertControllerStyle.Alert);
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(defaultAction);
                btnPressed.backgroundColor = UIColor.darkGrayColor();
                btnPressed.tag = 1;
                if(questionsWrong == 3){
                    self.performSegueWithIdentifier("GameOverSegue", sender: self);
                }
                presentViewController(alert, animated: true, completion: nil);
            }else{
                for(var i = 0; i < buttonArray.count; i++){
                    buttonArray[i].enabled = true;
                }
            }
        }
    }
    // Loads a new question while considering the ones already asked - end game if all questions were asked
    func loadQuestion(){
        if(!jokerWasUsed){ jokerButton.enabled = true; }
        var index = self.randRange(0, upper: questionList.count-1);
        while(alreadyAsked.contains(index)){
            index = self.randRange(0, upper: questionList.count-1);
        }
        alreadyAsked.append(index);
        if(questionList.count == alreadyAsked.count){
            self.performSegueWithIdentifier("GameOverSegue", sender: self);
            print("All questions asked");
        }else{
            let question:Question = questionList[index];
            currentQuestion = question;
            let answers = question.getAnswers();
            questionLabel.text = question.getQuestionTitle();
            setButtonText(answers);
            progressLabel.text = String(alreadyAsked.count) + "/" + String(questionList.count);
        }
    }
    // The user wants to use his joker - set Button to unabled afterwards
    // Choose two wrong answers by random and set them to unable
    @IBAction func useJoker(sender: AnyObject) {
        var cnt = 0;
        let answers = currentQuestion.getAnswers();
        while(cnt != 2){
            let index = randRange(0, upper: 3);
            if(answers[index].getIsRight() == false){
                if(buttonArray[index].tag != 1){
                    buttonArray[index].backgroundColor = UIColor.darkGrayColor();
                    // Tag is 1 if the button is disabled
                    buttonArray[index].tag = 1;
                    cnt++;
                }
            }
        }
        jokerWasUsed = true;
        jokerButton.enabled = false;
    }
    // Sets the text of the buttons according to the answers provided
    func setButtonText(answers:Array<Answer>){
        for(var i = 0; i < answers.count; i++){
            buttonArray[i].setTitle(answers[i].getText(), forState: UIControlState.Normal);
            buttonArray[i].tag = 0;
            buttonArray[i].backgroundColor = UIColor.lightGrayColor();
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "GameOverSegue"){
            let controller:GameOverViewController = segue.destinationViewController as! GameOverViewController;
            controller.questionList = self.questionList;
            // Check if the segue is called because the user won/lost
            if(questionsWrong == 3){
                controller.hasWon = false;
            }else{
                controller.hasWon = true;
            }
        }
    }

    
    // Gets the random values needed for getting a question
    func randRange (lower: Int , upper: Int) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
    }
    
}