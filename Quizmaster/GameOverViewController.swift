//
//  GameOverViewController.swift
//  Quizmaster
//
//  Created by Ralph Schnalzenberger on 22.11.15.
//  Copyright © 2015 Ralph Schnalzenberger. All rights reserved.
//


import Foundation
import UIKit

class GameOverViewController: UIViewController {
    var hasWon:Bool = false;
    var questionList:Array<Question> = [];
    @IBOutlet weak var messageLabel: UILabel!;
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMessage();
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
    }
    func loadMessage(){
        if(hasWon){
            messageLabel.text = "Gratulation! Alle Fragen wurden richtig beantwortet!";
        }else{
            messageLabel.text = "Diesmal war's nichts! Viel Glück beim nächsten Mal!";
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "startAgainSegue"){
            let controller:GameViewController = segue.destinationViewController as! GameViewController;
            controller.questionList = self.questionList;
        }
    }
}