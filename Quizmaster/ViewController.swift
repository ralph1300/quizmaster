//
//  ViewController.swift
//  Quizmaster
//
//  Created by Ralph Schnalzenberger on 16.11.15.
//  Copyright © 2015 Ralph Schnalzenberger. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // The array that will be filled with the questions
    var questionList:Array<Question> = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadQuestions();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadQuestions(){
        let endPoint = "https://html-sfraungruber.rhcloud.com/Ralph/json.json";
        guard let url = NSURL(string: endPoint) else {
            print("Error: cannot create URL");
            return;
        }
        // Create a NSURLSession with an URLRequest an fetch the data
        let urlRequest = NSURLRequest(URL: url);
        let config = NSURLSessionConfiguration.defaultSessionConfiguration();
        let session = NSURLSession(configuration: config);
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data");
                return;
            }
            guard error == nil else {
                print(error);
                return;
            }
            // Parse the result as JSON
            let response: NSArray?
            do {
                response = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as? NSArray;
                self.questionList = self.parseToQuestions(response!);
            } catch  {
                print("error trying to convert data to JSON");
                return;
            }
        });
        task.resume();
    }
    
    func parseToQuestions(arr:NSArray)->Array<Question>{
        var parsedArray:Array<Question> = [];
        // For each element in the Array, parse a question
        for(var i = 0; i < arr.count; i++){
            // Get the dictionary with the question/answers
            let dict:NSDictionary = arr[i] as! NSDictionary;
            // Create variables for the answers and the question
            var answers:Array<Answer> = [];
            let q:Question = Question.init();
            // Set the title from the dictionary
            q.setQuestionTitle(dict["questionTitle"] as! String);
            // Get the answers out of the dictionary -> are also a dictionary
            let answerDictArray:NSArray = dict["answers"] as! NSArray;
            //Parse the answer dictionaries to the needed objects
            for(var j = 0; j < answerDictArray.count; j++){
                let d:NSDictionary = answerDictArray[j] as! NSDictionary;
                let answer:Answer = Answer.init();
                answer.setText(d["text"] as! String);
                
                // The value of true/false from the json are 0/1 -> alter them to true/false
                if((d["isRight"] as! Int) == 0){
                    answer.setIsRight(false);
                }else{
                    answer.setIsRight(true);
                }
                answers.append(answer);
            }
            // Add the parsed answers to the question and append it to the array
            q.setAnswers(answers);
            parsedArray.append(q);
        }
        return parsedArray;
    }
    
    // This method is called when a "segue" happens - this call transfers our parsed questionList to the GameViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "startGameSegue"){
            let controller:GameViewController = segue.destinationViewController as! GameViewController;
            // This attribute has to be defined in GameViewController
            controller.questionList = self.questionList;
        }
    }


}















