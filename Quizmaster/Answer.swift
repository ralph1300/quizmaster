//
//  Answer.swift
//  Quizmaster
//
//  Created by Ralph Schnalzenberger on 16.11.15.
//  Copyright © 2015 Ralph Schnalzenberger. All rights reserved.
//

import Foundation

//Answer class, used in the question class
class Answer {
    private var text:String;
    private var isRight:Bool;
    //constructor with given values
    init(_text:String,_isRight:Bool){
        text = _text;
        isRight = _isRight;
    }
    //default constructor, 
    //uses given default values for creating the object
    init(){
        text = "";
        isRight = false;
    }
    //getter and setter
    func getText()->String{ return text; }
    func setText(_text:String){ text = _text; }
    func getIsRight()->Bool{ return isRight; }
    func setIsRight(_isRight:Bool){ isRight = _isRight; }
}